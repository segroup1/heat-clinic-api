/**
 * Module dependencies.
 */
var express = require('express');
var mysql = require('mysql');
var cons = require('consolidate');
var swig = require('swig');

var app = module.exports = express();

var db_config = {
    host: 'us-cdbr-east-04.cleardb.com',
    user: 'bd9dea8ab54d0b',
    password: '0754c374',
    database: 'heroku_c1077be62317484',
    connectionLimit: 10,
    supportBigNumbers: true
};

var pool = mysql.createPool(db_config);

app.configure(function(){
	app.set('port', process.env.PORT);

	app.engine('html', cons.swig);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'html');

	app.set('db', pool);

	app.use(express.favicon());
	app.use(express.logger('dev'));

	app.use(express.bodyParser());

	app.use(express.static(__dirname + '/public'), {maxAge: 1});

	app.use(express.methodOverride());
	app.use(app.router);
	app.use(express.errorHandler());

	require('./routes/api')(app);
});