var category = require('../controllers/category');
var product = require('../controllers/product');
var offer = require('../controllers/offer');
var media = require('../controllers/media');
var page = require('../controllers/page');
var sku = require('../controllers/sku');

module.exports = function(app){
	// help page
	app.get('/', page.index);

	// database tables and descriptions
	app.get('/tables', page.tables);
	app.get('/table/:t_name', page.table);

	// products
	app.get('/products', product.index);
	app.get('/product/:id', product.item);
	app.post('/product', product.create);
	app.put('/product/:id', product.update);
	app.delete('/product/:id', product.delete);

	// categories
	app.get('/categories', category.index);
	app.get('/category/:id', category.item);
	app.post('/category', category.create);
	app.put('/category/:id', category.update);
	app.delete('/category/:id', category.delete);

	// offers
	app.get('/offers', offer.index);
	app.get('/offer/:id', offer.item);
	app.post('/offer', offer.create);
	app.put('/offer/:id', offer.update);
	app.delete('/offer/:id', offer.delete);

	// media
	app.get('/media', media.index);
	app.get('/media/:id', media.item);
	app.post('/media', media.create);
	app.put('/media/:id', media.update);
	app.delete('/media/:id', media.delete);

	// sku cross reference
	app.get('/skus', sku.index);
	app.get('/sku/:id', sku.item);

	// error handling
	app.get('/:error', page.error);
};