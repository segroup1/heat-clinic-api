var app = require('../index');
var db = app.get('db');

exports.index = function(req, res, next){
	db.getConnection(function(err, connection){
		if(err){
			console.log('error: ', err);
		}

		connection.query('select * from blc_media', function(err, rows, fields){
			if(err){
				console.log('error: ', err);
			}

			res.send(rows);
		});
	});
};

exports.item = function(req, res, next){
	db.getConnection(function(err, connection){
		if(err){
			console.log('error', err);
		}

		connection.query('select * from blc_media where media_id = ' + req.params.id, function(err, rows, fields){
			if(err){
				console.log('error: ', err);
			}

			if(rows.length < 1){
				return res.send({});
			} else {
				return res.send(rows[0]);
			}
		});
	});
};

exports.create = function(req, res, next){
	db.getConnection(function(err, connection){
		if(err){
			console.log('error', err);
		}

		var str1 = "";
		var str2 = "";
		var first = true;
		Object.keys(req.body).forEach(function(key){
			if(!first){
				str1 += ", ";
				str2 += ", ";
			}

			str1 += key;
			str2 += "'" + req.body[key] + "'";

			if(first){
				first = false;
			}
		});

		if(str1.length > 0 && str2.length > 0){
			connection.query("insert into blc_media (" + str1 + ") values(" + str2 + ")", function(err, rows, fields){
				if(err){
					console.log('error: ', err);
				}

				return res.send("creation successful");
			});
		} else {
			res.send("creation failed");
		}
	});
};

exports.update = function(req, res, next){
	db.getConnection(function(err, connection){
		if(err){
			console.log('error', err);
		}

		var str = "";
		var first = true;
		Object.keys(req.body).forEach(function(key){
			if(!first){
				str += ", ";
			}

			str += key + "='" + req.body[key] + "'";

			if(first){
				first = false;
			}
		});

		if(str.length > 0){
			connection.query('update blc_media set ' + str + ' where media_id = ' + req.params.id, function(err, rows, fields){
				if(err){
					console.log('error: ', err);
				}

				return res.send("update successful");
			});
		} else {
			res.send("update failed");
		}
	});
};

exports.delete = function(req, res, next){
	db.getConnection(function(err, connection){
		if(err){
			console.log('error', err);
		}

		connection.query('delete from blc_media where media_id = ' + req.params.id, function(err, rows, fields){
			if(err){
				return res.send("delete failed");
				console.log('error: ', err);
			}

			return res.send("delete successful");
		});
	});
};