var app = require('../index');
var db = app.get('db');

exports.index = function(req, res, next){
	res.render('index.html');
};

exports.tables = function(req, res, next){
	db.getConnection(function(err, connection) {
		if(err){
			console.log('error: ', err);
		}

		connection.query('show Tables', function(err, rows, fields) {
	        if (err) {
	            console.log('error: ', err);
	        }

	        res.send(rows);
	    });
	});
};

exports.table = function(req, res, next){
	db.getConnection(function(err, connection) {
		if(err){
			console.log('error: ', err);
		}

		connection.query('describe ' + req.params.t_name, function(err, rows, fields){
			if(err){
				console.log('error: ', err);
			}

			res.send(rows);
		});
	});
};

exports.error = function(req, res, next){
	res.send({
        error:'Not Found',
        status:404
    });
};