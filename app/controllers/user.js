var app = require('../index');
var db = app.get('db');

exports.index = function(req, res, next){
	db.getConnection(function(err, connection){
		if(err){
			console.log('error: ', err);
		}

		connection.query('select * from blc_admin_user', function(err, rows, fields){
			if(err){
				console.log('error: ', err);
			}

			res.send(rows);
		});
	});
};

exports.item = function(req, res, next){
	db.getConnection(function(err, connection){
		if(err){
			console.log('error', err);
		}

		connection.query('select * from blc_admin_user where admin_user_id = ' + req.params.id, function(err, rows, fields){
			if(err){
				console.log('error: ', err);
			}

			if(rows.length < 1){
				return res.send({});
			} else {
				return res.send(rows[0]);
			}
		});
	});
};