var app = require('../index');
var db = app.get('db');

exports.index = function(req, res, next){
	db.getConnection(function(err, connection){
		if(err){
			console.log('error: ', err);
		}

		connection.query('select sku_id from blc_product_sku_xref', function(err, rows, fields){
			if(err){
				console.log('error: ', err);
			}

			res.send(rows);
		});
	});
};

exports.item = function(req, res, next){
	db.getConnection(function(err, connection){
		if(err){
			console.log('error', err);
		}

		connection.query('select * from blc_product where product_id = (select product_id from blc_product_sku_xref where sku_id = ' + req.params.id + ')', function(err, rows, fields){
			if(err){
				console.log('error: ', err);
			}

			if(rows.length < 1){
				return res.send({});
			} else {
				return res.send(rows[0]);
			}
		});
	});
};