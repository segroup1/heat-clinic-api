var http = require('http');
var app = require('./app');

var startServer = function(){
	http.createServer(app).listen(app.get('port'), function(){
		console.log("Express server listening on port "+app.get('port'));
	});
};

if(app.settings.env === 'development'){
	startServer();
} else {
    var cluster = require('cluster');
    var cores = require('os').cpus().length;

    if (cluster.isMaster) {
        console.log('starting master: ' + process.pid);
        console.log(process.versions);

        for (i = 0; i < cores; i++) {
            cluster.fork();
        }

        cluster.on('death', function (worker) {
            return console.log('worker ' + worker.pid + ' died');
        });
    } else {
        console.log('starting worker: ' + process.pid);
        startServer();
    }
}